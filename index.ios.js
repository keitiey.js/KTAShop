import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import App from './src/Components/App';

export default class KTAShop extends Component {
  render() {
    return (
      <App />
    );
  }
}

AppRegistry.registerComponent('KTAShop', () => KTAShop);
