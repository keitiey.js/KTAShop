/*
    Shop.js
        + addProductToCart
        + cartArray
        + incrQuantity
        + decrQuantity
        + removeProduct
        + gotoSearchTab
    Menu.js
        + onLogin
        + onLogout
*/
const toTitleCase = (str) => 
    str.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
    
module.exports = {
    toTitleCase,
    addProductToCart: null,
    cartArray: [],
    incrQuantity: null,
    decrQuantity: null,
    removeProduct: null,
    onLogin: null,
    onLogout: null,
    gotoSearchTab: null
};

