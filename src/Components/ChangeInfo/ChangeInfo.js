import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    Alert
} from 'react-native';

//  STYLES
import { sChangeInfo } from '../../Styles/rootStyles';
//  IMAGES
import icBacks from '../../Media/appIcon/backs.png';
//  API
import changeInfo from '../../API/changeInfo';
//  GLOBAL
import global from '../global';

export default class ChangeInfo extends Component {
    constructor(props) {
        super(props);
        const { name, address, phone } = props.navigation.state.params.user;
        this.state = {
            name,
            address,
            phone
        };
    }
    changeInfoTemp() {
        const { token } = this.props.navigation.state.params;
        const { name, address, phone } = this.state;
        changeInfo(token, name, address, phone)
            .then(user => {
                Alert.alert(
                    'Notice',
                    'Change infomation successfully',
                    [
                        { text: 'OK', 
                            onPress: () => { 
                                global.onLogin({ token, user }); 
                                this.props.navigation.goBack();
                            } 
                        },
                    ],
                    { cancelable: false }
                );
            })
            .catch(err => console.log(err));
    }
    render() {
        const {
            container, header, txtHeader, iconHeader, body,
            inputText, wrapInput, btnChange
        } = sChangeInfo;
        // const { name, address, phone } = this.props.navigation.state.params;
        return (
            <View style={container}>
                <View style={header}>
                    <View />
                    <Text style={txtHeader}>User Infomation</Text>
                    <TouchableOpacity>
                        <Image source={icBacks} style={iconHeader} />
                    </TouchableOpacity>
                </View>
                <View style={body}>
                    <View style={wrapInput}>
                        <TextInput
                            underlineColorAndroid='transparent'
                            style={inputText}
                            value={this.state.name}
                            placeholder="Your name"
                            onChangeText={text => this.setState({ name: text })}
                        />
                        <TextInput
                            underlineColorAndroid='transparent'
                            style={inputText}
                            value={this.state.address}
                            placeholder="Your address"
                            onChangeText={text => this.setState({ address: text })}
                        />
                        <TextInput
                            underlineColorAndroid='transparent'
                            style={inputText}
                            value={this.state.phone}
                            placeholder="Your phone"
                            onChangeText={text => this.setState({ phone: text })}
                        />
                        <TouchableOpacity
                            style={btnChange}
                            onPress={() => { this.changeInfoTemp(); }}
                        >
                            <Text style={{ color: '#FFFFFF' }}>CHANGE YOUR INFOMATION</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
