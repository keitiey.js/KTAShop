import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';

//  COMPONENTS
import SignIn from './SignIn';
import SignUp from './SignUp';

//  STYLES
import { sAuthentication } from '../../Styles/ChildStyles/sAuthentication';

//  IMAGES
import icLogo from '../../Media/appIcon/ic_logo.png';
import icBack from '../../Media/appIcon/back_white.png';

export default class Authentication extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSignIn: true
        };
    }
    gotoSignIn() {
        this.setState({ isSignIn: true });
    }
    signIn() {
        this.setState({ isSignIn: true });
    }
    signUp() {
        this.setState({ isSignIn: false });
    }
    backToMain() {
        const { navigation } = this.props;
        navigation.goBack();
    }
    render() {
        const { wrapper, row1,
            titleStyle, iconStyle, wrapControl,
            signInStyle, signUpStyle, activeStyle, inactiveStyle
        } = sAuthentication;
        const { isSignIn } = this.state;
        const mainJSX = isSignIn ? 
            <SignIn backToMain={this.backToMain.bind(this)} /> : 
            <SignUp gotoSignIn={this.gotoSignIn.bind(this)} />;
        return (
            <View style={wrapper}>
                <View style={row1}>
                    <TouchableOpacity onPress={this.backToMain.bind(this)}>
                        <Image source={icBack} style={iconStyle} />
                    </TouchableOpacity>
                    <Text style={titleStyle}>Wearing a Dress</Text>
                    <Image source={icLogo} style={iconStyle} />
                </View>
                {mainJSX}
                <View style={wrapControl}>
                    <TouchableOpacity style={signInStyle} onPress={this.signIn.bind(this)}>
                        <Text style={isSignIn ? activeStyle : inactiveStyle}>SIGN IN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={signUpStyle} onPress={this.signUp.bind(this)}>
                        <Text style={!isSignIn ? activeStyle : inactiveStyle}>SIGN UP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
