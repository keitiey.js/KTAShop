import React, { Component } from 'react';
import {
    Text,
    TextInput,
    TouchableOpacity,
    Animated,
    Alert
} from 'react-native';

//  STYLES
import { sAuthentication } from '../../Styles/ChildStyles/sAuthentication';

// API
import register from '../../API/register';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity: new Animated.Value(0),
            email: '',
            name: '',
            password: '',
            rePassword: ''
        };
    }
    componentDidMount() {
        Animated.timing(
            this.state.opacity,
            {
                toValue: 1,
                duration: 1000
            }
        ).start();
    }
    onSuccess() {
        // Works on both iOS and Android
        Alert.alert(
            'Notice',
            'Sign up successfuly',
            [
                { text: 'OK', onPress: () => this.props.gotoSignIn() },
            ],
            { cancelable: false }
        );
    }
    onFail() {
        // Works on both iOS and Android
        Alert.alert(
            'Notice',
            'Sign up unsuccessfuly',
            [
                { text: 'OK', onPress: () => this.setState({ email: '' }) },
            ],
            { cancelable: false }
        );        
    }
    isRegister() {
        const { email, name, password } = this.state;
        register(email, name, password)
        .then(res => {
            if (res === 'THANH_CONG') return this.onSuccess();
            return this.onFail();
        });
    }
    render() {
        const { opacity } = this.state;
        const { inputStyle, bigButton, buttonText } = sAuthentication;
        return (
            <Animated.View style={{ opacity }}>
                <TextInput
                    underlineColorAndroid='transparent'
                    style={inputStyle} placeholder="Enter your name"
                    value={this.state.name}
                    onChangeText={text => this.setState({ name: text })}
                />
                <TextInput
                    underlineColorAndroid='transparent'
                    style={inputStyle} placeholder="Enter your email"
                    value={this.state.email}
                    onChangeText={text => this.setState({ email: text })}
                />
                <TextInput
                    underlineColorAndroid='transparent'
                    style={inputStyle} placeholder="Enter your password"
                    value={this.state.password}
                    onChangeText={text => this.setState({ password: text })}
                    secureTextEntry
                />
                <TextInput
                    underlineColorAndroid='transparent'
                    style={inputStyle} placeholder="Re-enter your password"
                    value={this.state.rePassword}
                    onChangeText={text => this.setState({ rePassword: text })}
                    secureTextEntry
                />
                <TouchableOpacity 
                    style={bigButton}
                    onPress={this.isRegister.bind(this)}
                >
                    <Text style={buttonText}>SIGN UP NOW</Text>
                </TouchableOpacity>
            </Animated.View>
        );
    }
}

module.exports = SignUp;
