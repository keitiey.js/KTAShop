import React, { Component } from 'react';
import {
    Text,
    TextInput,
    TouchableOpacity,
    Animated
} from 'react-native';

//  STYLES
import { sAuthentication } from '../../Styles/ChildStyles/sAuthentication';

//  API
import onSignIn from '../../API/onSignIn';
import saveToken from '../../API/saveToken';

//  GLOBAL
import global from '../global';

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity: new Animated.Value(0),
            email: '',
            password: ''
        };
    }
    componentDidMount() {
        Animated.timing(
            this.state.opacity,
            {
                toValue: 1,
                duration: 1000
            }
        ).start();
    }
    onSignInTemp() {
        const { email, password } = this.state;
        onSignIn(email, password)
        .then(res => {
            global.onLogin(res.user);
            saveToken(res.token);
            this.props.backToMain();
        })
        .catch(err => console.log(err));
    }
    render() {
        const { opacity } = this.state;
        const { inputStyle, bigButton, buttonText } = sAuthentication;
        return (
            <Animated.View style={{ opacity }}>
                <TextInput
                    underlineColorAndroid='transparent'
                    style={inputStyle}
                    placeholder="Enter your email"
                    value={this.state.email}
                    onChangeText={text => { this.setState({ email: text }); }}
                />
                <TextInput
                    underlineColorAndroid='transparent'
                    style={inputStyle}
                    placeholder="Enter your password"
                    value={this.state.password}
                    onChangeText={text => { this.setState({ password: text }); }}
                    secureTextEntry
                />
                <TouchableOpacity 
                    style={bigButton}
                    onPress={() => { this.onSignInTemp(); }}
                >
                    <Text style={buttonText}>SIGN IN NOW</Text>
                </TouchableOpacity>
            </Animated.View>
        );
    }
}

module.exports = SignIn;
