import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';

//  STYLES
import { sMenu } from '../../../Styles/rootStyles';

class Logout extends Component {
    render() {
        const { buttonStyle, buttonText } = sMenu;
        return (
            <View>
                <TouchableOpacity
                    style={buttonStyle}
                    onPress={() => { this.props.screenProps.goToAuthentication(); }}
                >
                    <Text style={buttonText}>Sign In</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

module.exports = Logout;
