import React, { Component } from 'react';

import {
    View,
    Image
} from 'react-native';
//  COMPONENTS
import Login from './Login';
import Logout from './Logout';

//  STYLES
import { sMenu } from '../../../Styles/rootStyles';
//  IMAGES
import profileIcon from '../../../Media/temp/profile.png';
//  GLOBAL
import global from '../../global';

export default class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = { user: null };
        global.onLogin = this.onLogin.bind(this);
        global.onLogout = this.onLogout.bind(this);
    }
    onLogin(user) {
        this.setState({ user });
    }
    onLogout() {
        this.setState({ user: null });
    }
    render() {
        const {
            container,
            profileImg
        } = sMenu;
        const { screenProps } = this.props;
        const { user } = this.state;
        const mainJSX = user 
            ? <Login user={user} screenProps={screenProps} /> 
            : <Logout screenProps={screenProps} />;
        return (
            <View style={container}>
                <Image
                    source={profileIcon}
                    style={profileImg}
                />
                {mainJSX}
            </View>
        );
    }
}
