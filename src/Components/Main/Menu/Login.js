import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';

//  STYLES
import { sMenu } from '../../../Styles/rootStyles';

//  GLOBAL
import global from '../../global';

//  API
import saveToken from '../../../API/saveToken';

class Login extends Component {
    onLogout() {
        saveToken('');
        global.onLogout();
    }
    render() {
        const { 
            loginContainer, usernameStyle, buttonStyleSignIn,
            buttonTextSignIn      
        } = sMenu;
        const { user } = this.props;
        return (
            <View style={loginContainer}>
                <Text style={usernameStyle}>{user.name}</Text>
                <View>
                    <TouchableOpacity
                        style={buttonStyleSignIn}
                        onPress={() => { this.props.screenProps.goToOrderHistory(); }}
                    >
                        <Text style={buttonTextSignIn}>Order History</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={buttonStyleSignIn}
                        onPress={() => { this.props.screenProps.goToChangeInfo(user); }}
                    >
                        <Text style={buttonTextSignIn}>Change Info</Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        style={buttonStyleSignIn}
                        onPress={() => { this.onLogout(); }}
                    >
                        <Text style={buttonTextSignIn}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
                <View />
            </View>
        );
    }
}

module.exports = Login;
