import React, { Component } from 'react';
import { DrawerShopToMenu } from '../../Navigation/Navigation';

//  API
import getToken from '../../API/getToken';
import checkLogin from '../../API/checkLogin';

// GLOBAL
import global from '../global';

export default class Main extends Component {
    componentDidMount() {
        getToken()
        .then(token => 
            checkLogin(token)
            .then(user => global.onLogin(user))
        )    
        .catch(err => console.log('checkLogin: ', err));
    }
    goToAuthentication() {
        const { navigation } = this.props;
        navigation.navigate('MH_Authentication');
    }
    goToChangeInfo(user) {
        const { navigation } = this.props;
        navigation.navigate('MH_ChangeInfo', user);
    }
    goToOrderHistory() {
        const { navigation } = this.props;
        navigation.navigate('MH_OrderHistory');
    }
    render() {
        return (
            <DrawerShopToMenu 
                screenProps={{
                    goToAuthentication: this.goToAuthentication.bind(this),
                    goToChangeInfo: this.goToChangeInfo.bind(this),
                    goToOrderHistory: this.goToOrderHistory.bind(this),
                }}
            />
        );
    }
}
