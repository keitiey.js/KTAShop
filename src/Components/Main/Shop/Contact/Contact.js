import React, { Component } from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';

//  STYLES
import { sContact } from '../../../../Styles/rootStyles';
//  IMAGES
import map from '../../../../Media/appIcon/map.png';
import icLocation from '../../../../Media/appIcon/location.png';
import icPhone from '../../../../Media/appIcon/phone.png';
import icMail from '../../../../Media/appIcon/mail.png';
import icMessage from '../../../../Media/appIcon/message.png';

export default class Contact extends Component {
    render() {
        const {
            container, wrapMap, mapStyle, wrapInfo, iconStyle, txtStyle,
            wrapRow, wrapLastRow
        } = sContact;
        return (
            <View style={container}>
                <View style={wrapMap}>
                    <Image
                        source={map}
                        style={mapStyle}
                    />
                </View>
                <View style={wrapInfo}>
                    <View style={wrapRow}>
                        <Image
                            source={icLocation}
                            style={iconStyle}
                        />
                        <Text style={txtStyle}>90 Le Thi Rieng/ Ben Thanh</Text>
                    </View>
                    <View style={wrapRow}>
                        <Image
                            source={icPhone}
                            style={iconStyle}
                        />
                        <Text style={txtStyle}>0967022594</Text>
                    </View>
                    <View style={wrapRow}>
                        <Image
                            source={icMail}
                            style={iconStyle}
                        />
                        <Text style={txtStyle}>keitiey.js@gmail.com</Text>
                    </View>
                    <View style={wrapLastRow}>
                        <Image
                            source={icMessage}
                            style={iconStyle}
                        />
                        <Text style={txtStyle}>0967022594</Text>
                    </View>
                </View>
            </View>
        );
    }
}

