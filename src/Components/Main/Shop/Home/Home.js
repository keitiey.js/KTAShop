import React, { Component } from 'react';
import {
    ScrollView
} from 'react-native';

import Collection from './ChildComponents/Collection';
import Category from './ChildComponents/Category';
import TopProduct from './ChildComponents/TopProduct';

//  GLOBAL
import global from '../../../global';

export default class Home extends Component {
    constructor(props) {
        super(props);
        global.gotoSearchTab = this.gotoSearchTab.bind(this);
    }
    gotoSearchTab(listSearch) {
        const { navigation } = this.props;
        navigation.navigate('MH_Search', listSearch);
    }
    gotoListProduct(idType, nameList) {
        const { navigation } = this.props;
        navigation.navigate('MH_ListProduct', { idType, nameList });
    }
    gotoDetail(product) {
        const { navigation } = this.props;
        if (product == null) navigation.navigate('MH_ProductDetail');
        else navigation.navigate('MH_ProductDetail', product);
    }
    goBack() {
        const { navigation } = this.props;
        navigation.goback();
    }
    render() {
        const { types, products } = this.props.screenProps;
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#DFDEDC' }} >
                <Collection />
                <Category 
                    types={types}
                    gotoListProduct={this.gotoListProduct.bind(this)}
                />
                <TopProduct 
                    products={products}
                    gotoDetail={this.gotoDetail.bind(this)}
                />
            </ScrollView>
        );
    }
}
