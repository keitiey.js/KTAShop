import React, { Component } from 'react';
import {  
    View, 
    Text,
    Image
} from 'react-native';

//  STYLES
import { sCollection } from '../../../../../Styles/rootStyles';
import banner from '../../../../../Media/temp/banner.jpg';

class Collection extends Component {
    render() {
        const { wrapper, textStyle, imageStyle } = sCollection;
        return (
            <View style={wrapper}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={textStyle}>SPRING COLLECTION</Text>
                </View>
                <View style={{ flex: 5 }}>
                    <Image 
                        source={banner}
                        style={imageStyle}
                    />
                </View>
            </View>
        );
    }
}

module.exports = Collection;
