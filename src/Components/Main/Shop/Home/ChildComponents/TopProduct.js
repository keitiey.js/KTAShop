import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ListView
} from 'react-native';

//  STYLES
import { sTopProduct } from '../../../../../Styles/rootStyles';
import { width } from '../../../../../Styles/constStyles';

//  CONFIGURE
import { urlImgProduct } from '../../../../../configure';

class TopProduct extends Component {
    render() {
        const {
            container,
            titleContainer,
            title,
            bodyContainer,
            productContainer,
            productImg,
            productName,
            productPrice
        } = sTopProduct;
        const { products } = this.props;
        return (
            <View style={container}>
                <View style={titleContainer}>
                    <Text style={title}>TOP PRODUCT</Text>
                </View>
                <ListView
                    enableEmptySections
                    contentContainerStyle={bodyContainer}
                    dataSource={
                        new ListView.DataSource({ rowHasChanged: (r1, r2) => 
                            r1 !== r2 }).cloneWithRows(products)
                    }
                    renderRow={(product) => (
                        <TouchableOpacity
                            style={productContainer}
                            onPress={() => { this.props.gotoDetail(product); }}
                        >
                            <Image
                                source={{ uri: `${urlImgProduct}/${product.images[0]}` }}
                                style={productImg}
                            />
                            <Text style={productName}>{product.name.toUpperCase()}</Text>
                            <Text style={productPrice}>{product.price}$</Text>
                        </TouchableOpacity>
                    )}
                    renderSeparator={(selectionID, rowID) => {
                        if (rowID % 2 === 1) return <View style={{ width, height: 10 }} />;
                        return null;
                    }}
                />
            </View>
        );
    }
}

module.exports = TopProduct;

/* 
<View style={bodyContainer}>
{ products.map(e => (
    <TouchableOpacity
        key={e.id}
        style={productContainer}
        onPress={() => { this.props.gotoDetail(e); }}
    >
        <Image 
            source={{ uri: `${urlImgProduct}/${e.images[0]}` }} 
            style={productImg} 
        />
        <Text style={productName}>{e.name.toUpperCase()}</Text>
        <Text style={productPrice}>{e.price}$</Text>
    </TouchableOpacity>
)) }
</View> 
*/
