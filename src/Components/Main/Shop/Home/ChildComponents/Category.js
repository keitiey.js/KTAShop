import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import Swiper from 'react-native-swiper';

//  STYLES
import { sCategory } from '../../../../../Styles/rootStyles';
import { category } from '../../../../../Styles/constStyles';

//  CONFIGURE
import { urlImgType } from '../../../../../configure';

class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleSwiper: false
        };
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({
                visibleSwiper: true
            });
        }, 100);
    }
    render() {
        const { wrapper, textStyle, imageStyle } = sCategory;
        const { types } = this.props;
        let swiper = null;
        if (this.state.visibleSwiper) {
            swiper = (
                <Swiper
                    dotColor={'white'}
                    activeDotColor={'#2ABB9C'}
                    horizontal
                    bounces
                    /* loop={true} */
                    width={category.widthImg}
                    height={category.heightImg}
                    removeClippedSubviews={false}
                >
                    { types.map(e => (
                        <View key={e.id}>
                            <TouchableOpacity 
                                onPress={() => { 
                                    this.props.gotoListProduct(e.id, e.name); 
                                }}
                            >
                                <Image 
                                    resizeMode='cover' 
                                    style={imageStyle} 
                                    source={{ uri: `${urlImgType}/${e.image}` }}
                                >
                                    <Text>{e.name}</Text>
                                </Image>
                            </TouchableOpacity>
                        </View>                        
                    )) }
                </Swiper>);
        } else {
            swiper = <View />;
        }
        return (
            <View style={wrapper}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={textStyle}>LIST OF CATEGORY</Text>
                </View>
                <View style={{ flex: 5 }}>
                    {swiper}
                </View>
            </View>
        );
    }
}

module.exports = Category;
