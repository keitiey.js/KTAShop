import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';

//  STYLES
import { sHeader } from '../../../Styles/rootStyles';

//  ICONS
import icMenu from '../../../Media/appIcon/ic_menu.png';
import icLogo from '../../../Media/appIcon/ic_logo.png';

//  GLOBAL
import global from '../../global';

//  API
import getSearchList from '../../../API/getSearchList';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            txtSearch: ''
        };
    }
    gotoSearchTab() {
        const { txtSearch } = this.state;
        getSearchList(txtSearch)
        .then(listSearch => { global.gotoSearchTab(listSearch); })
        .catch(err => console.log(err));
    }
    render() {
        const { 
            wrapper, 
            row1, 
            textInput, 
            iconStyle,
            titleStyle 
        } = sHeader;
        return (
            <View style={wrapper}>
                <View style={row1}>
                    <TouchableOpacity onPress={this.props.openMenu.bind(this)} >
                        <Image source={icMenu} style={iconStyle} />
                    </TouchableOpacity>
                    <Text style={titleStyle}>Wearing a Dress</Text>
                    <Image source={icLogo} style={iconStyle} />
                </View>
                <TextInput 
                    style={textInput} 
                    underlineColorAndroid='transparent'
                    placeholder="What do you want to buy?"
                    value={this.state.txtSearch}
                    onChangeText={text => { this.setState({ txtSearch: text }); }}
                    onSubmitEditing={this.gotoSearchTab.bind(this)}
                />
            </View>
        );
    }
}

module.exports = Header;
