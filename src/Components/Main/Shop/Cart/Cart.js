import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ListView,
    Image
} from 'react-native';

//  STYLES
import { sCart } from '../../../../Styles/rootStyles';

//  CONFIGURE
import { urlImgProduct } from '../../../../configure';

//  GLOBAL
import global from '../../../global';

export default class Cart extends Component {
    incrQuantity(productID) {
        global.incrQuantity(productID);
    }
    decrQuantity(productID) {
        global.decrQuantity(productID);
    }
    removeProduct(productID) {
        global.removeProduct(productID);
    }
    gotoProductDetail() {
        const { navigation } = this.props;
        navigation.navigate('MH_ProductDetail');
    }
    render() {
        const {
            container, listProduct, checkOutNow, txtCheckOutNow, wrapProduct,
            imgStyle, detailProduct, header, middle, bottom, txtName, txtPrice,
            txtShowDetails, txtControlNumber, wrapBtnShowDetails, wrapControlNumber
        } = sCart;
        const { cartArray } = this.props.screenProps;
        const arrTotal = cartArray.map(e => e.product.price * e.quantity); 
        const total = arrTotal.length 
            ? arrTotal.reduce((prevVal, currentVal) => prevVal + currentVal) : 0;
        return (
            <View style={container}>
                <View style={listProduct}>
                    <ListView
                        enableEmptySections
                        dataSource={
                            new ListView.DataSource({
                                rowHasChanged: (r1, r2) =>
                                    r1 !== r2
                            }).cloneWithRows(cartArray)
                        }
                        renderRow={cartItem => (
                            <View style={wrapProduct}>
                                <Image
                                    source={{ 
                                        uri: `${urlImgProduct}/${cartItem.product.images[0]}` 
                                    }}
                                    style={imgStyle}
                                />
                                <View style={detailProduct}>
                                    <View style={header}>
                                        <Text style={txtName}>
                                            {global.toTitleCase(cartItem.product.name)}
                                        </Text>
                                        <TouchableOpacity
                                            onPress={() => { 
                                                this.removeProduct(cartItem.product.id); 
                                            }}
                                        >
                                            <Text>X</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={middle}>
                                        <Text style={txtPrice}>{cartItem.product.price}$</Text>
                                    </View>
                                    <View style={bottom}>
                                        <View style={wrapControlNumber}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.decrQuantity(cartItem.product.id);
                                                }}
                                            >
                                                <Text style={txtControlNumber}>-</Text>
                                            </TouchableOpacity>
                                            <Text style={txtControlNumber}>
                                                {cartItem.quantity}
                                            </Text>
                                            <TouchableOpacity
                                                onPress={() => { 
                                                    this.incrQuantity(cartItem.product.id); 
                                                }}
                                            >
                                                <Text style={txtControlNumber}>+</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={wrapBtnShowDetails}>
                                            <TouchableOpacity>
                                                <Text style={txtShowDetails}>SHOW DETAILS</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        )}
                    />
                </View>
                <TouchableOpacity style={checkOutNow}>
                    <Text style={txtCheckOutNow}>TOTAL {total}$ CHECKOUT NOW</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
