import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    ListView, RefreshControl
} from 'react-native';

//  IMAGES
import icHome from '../../../../Media/appIcon/home.png';
import icBack from '../../../../Media/appIcon/backList.png';

//  STYLES
import { sIcon, sListProduct } from '../../../../Styles/rootStyles';

//  API
import getProductByType from '../../../../API/getProductByType';

//  CONFIGURE
import { urlImgProduct } from '../../../../configure';

//  GLOBAL
import global from '../../../global';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default class ListProduct extends Component {
    static navigationOptions = {
        tabBarLabel: 'Home',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={icHome}
                style={[sIcon, { tintColor }]}
            />
        )
    };
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            dataSource: ds,
            refreshing: false
        };
        this.arrProduct = [];
    }
    componentDidMount() {
        const { idType } = this.props.navigation.state.params;
        const { page } = this.state;
        getProductByType(idType, page)
            .then(arrProduct => {
                this.arrProduct = arrProduct.concat(this.arrProduct);
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(this.arrProduct),
                    page: this.state.page + 1
                });
            })
            .catch(err => console.log(err));
    }
    onRefresh() {
        const { idType } = this.props.navigation.state.params;
        this.setState({ refreshing: true });
        const newPage = this.state.page + 1;
        getProductByType(idType, newPage)
        .then(arrProduct => {
            this.arrProduct = arrProduct.concat(this.arrProduct);
            this.setState({
                refreshing: false,
                dataSource: this.state.dataSource.cloneWithRows(this.arrProduct),
                page: newPage
            });
        })
        .catch(err => {
            this.setState({ refreshing: false, page: this.state.page - 1 });
            console.log(err);
        });
    }
    goBack() {
        const { navigation } = this.props;
        navigation.goBack();
    }
    gotoDetail(product) {
        const { navigation } = this.props;
        navigation.navigate('MH_ProductDetail', product);
    }
    render() {
        const {
            container, wrapper, header,
            backStyle, titleStyle, productContainer,
            productInfo, productImage, lastRowInfo,
            txtName, txtPrice, txtMaterial, txtColor, txtShowDetail,

        } = sListProduct;
        const { nameList } = this.props.navigation.state.params;
        return (
            <View style={container}>
                <View style={header}>
                    <TouchableOpacity onPress={() => { this.goBack(); }}>
                        <Image
                            source={icBack}
                            style={backStyle}
                        />
                    </TouchableOpacity >
                    <Text style={titleStyle}>{nameList}</Text>
                    <View style={{ width: 30, height: 30 }} />
                </View>
                <ListView
                    enableEmptySections
                    contentContainerStyle={wrapper}
                    dataSource={this.state.dataSource}
                    renderRow={product => (
                        <View style={productContainer}>
                            <Image
                                source={{ uri: `${urlImgProduct}/${product.images[0]}` }}
                                style={productImage}
                            />
                            <View style={productInfo}>
                                <Text style={txtName}>{global.toTitleCase(product.name)}</Text>
                                <Text style={txtPrice}>{product.price}$</Text>
                                <Text style={txtMaterial}>Material {product.material}</Text>
                                <View style={lastRowInfo}>
                                    <Text style={txtColor}>Color {product.color}</Text>
                                    <View
                                        style={{
                                            backgroundColor: product.color.toLowerCase(),
                                            height: 16,
                                            width: 16,
                                            borderRadius: 8
                                        }}
                                    />
                                    <TouchableOpacity onPress={() => { this.gotoDetail(product); }}>
                                        <Text style={txtShowDetail}>SHOW DETAILS</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                />

            </View>
        );
    }
}

/*
<ScrollView style={wrapper}>
    <View style={header}>
        <TouchableOpacity onPress={() => { this.goBack(); }}>
            <Image
                source={icBack}
                style={backStyle}
            />
        </TouchableOpacity >
        <Text style={titleStyle}>Party Dress</Text>
        <View style={{ width: 30, height: 30 }} />
    </View>
    <View style={productContainer}>
        <Image
            source={sp1}
            style={productImage}
        />
        <View style={productInfo}>
            <Text style={txtName}>Lace Sleeve Si</Text>
            <Text style={txtPrice}>117$</Text>
            <Text style={txtMaterial}>Material Silk</Text>
            <View style={lastRowInfo}>
                <Text style={txtColor}>Color RoyalBlue</Text>
                <View
                    style={{
                        backgroundColor: 'cyan',
                        height: 16,
                        width: 16,
                        borderRadius: 8
                    }}
                />
                <TouchableOpacity onPress={() => { this.gotoDetail(); }}>
                    <Text style={txtShowDetail}>SHOW DETAILS</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    <View style={productContainer}>
        <Image
            source={sp1}
            style={productImage}
        />
        <View style={productInfo}>
            <Text style={txtName}>Lace Sleeve Si</Text>
            <Text style={txtPrice}>117$</Text>
            <Text style={txtMaterial}>Material Silk</Text>
            <View style={lastRowInfo}>
                <Text style={txtColor}>Color RoyalBlue</Text>
                <View
                    style={{
                        backgroundColor: 'cyan',
                        height: 16,
                        width: 16,
                        borderRadius: 8
                    }}
                />
                <TouchableOpacity onPress={() => { this.gotoDetail(); }}>
                    <Text style={txtShowDetail}>SHOW DETAILS</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    <View style={productContainer}>
        <Image
            source={sp1}
            style={productImage}
        />
        <View style={productInfo}>
            <Text style={txtName}>Lace Sleeve Si</Text>
            <Text style={txtPrice}>117$</Text>
            <Text style={txtMaterial}>Material Silk</Text>
            <View style={lastRowInfo}>
                <Text style={txtColor}>Color RoyalBlue</Text>
                <View
                    style={{
                        backgroundColor: 'cyan',
                        height: 16,
                        width: 16,
                        borderRadius: 8
                    }}
                />
                <TouchableOpacity onPress={() => { this.gotoDetail(); }}>
                    <Text style={txtShowDetail}>SHOW DETAILS</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    <View style={productContainer}>
        <Image
            source={sp1}
            style={productImage}
        />
        <View style={productInfo}>
            <Text style={txtName}>Lace Sleeve Si</Text>
            <Text style={txtPrice}>117$</Text>
            <Text style={txtMaterial}>Material Silk</Text>
            <View style={lastRowInfo}>
                <Text style={txtColor}>Color RoyalBlue</Text>
                <View
                    style={{
                        backgroundColor: 'cyan',
                        height: 16,
                        width: 16,
                        borderRadius: 8
                    }}
                />
                <TouchableOpacity onPress={() => { this.gotoDetail(); }}>
                    <Text style={txtShowDetail}>SHOW DETAILS</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    <View style={productContainer}>
        <Image
            source={sp1}
            style={productImage}
        />
        <View style={productInfo}>
            <Text style={txtName}>Lace Sleeve Si</Text>
            <Text style={txtPrice}>117$</Text>
            <Text style={txtMaterial}>Material Silk</Text>
            <View style={lastRowInfo}>
                <Text style={txtColor}>Color RoyalBlue</Text>
                <View
                    style={{
                        backgroundColor: 'cyan',
                        height: 16,
                        width: 16,
                        borderRadius: 8
                    }}
                />
                <TouchableOpacity onPress={() => { this.gotoDetail(); }}>
                    <Text style={txtShowDetail}>SHOW DETAILS</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
</ScrollView>
*/
