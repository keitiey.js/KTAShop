import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
//  IMAGES
import icBack from '../../../../Media/appIcon/back.png';
import icCartFull from '../../../../Media/appIcon/cartfull.png';
//  STYLES
import { sProductDetail } from '../../../../Styles/rootStyles';
//  CONFIGURE
import { urlImgProduct } from '../../../../configure';
//  GLOBAL
import global from '../../../global';

export default class ProductDetail extends Component {
    addThisProductToCart() {
        try {
            const tempProduct = this.props.navigation.state.params;
            global.addProductToCart(tempProduct);
        } catch (error) {
            console.error(error);
        }
    }
    goBack() {
        const { navigation } = this.props;
        navigation.goBack();
    }
    render() {
        const {
            container, wrapper, header, imageProduct, content,
            btnBack, btnCartFull, imgStyle, infoProduct,
            nameProduct, priceProduct, wrapInfoProduct, descProduct,
            txtDescProduct, bottomProduct, rightBottomProduct, txtName,
            txtColor
        } = sProductDetail;
        const product = this.props.navigation.state.params;
        return (
            <View style={container}>
                {product == null ? <View /> :
                    <ScrollView style={wrapper}>
                        <View style={header}>
                            <TouchableOpacity onPress={() => { this.goBack(); }}>
                                <Image source={icBack} style={btnBack} />
                            </TouchableOpacity>
                            <View />
                            <TouchableOpacity
                                onPress={this.addThisProductToCart.bind(this)}
                            >
                                <Image source={icCartFull} style={btnCartFull} />
                            </TouchableOpacity>
                        </View>
                        <View style={content}>
                            <ScrollView style={imageProduct} horizontal>
                                {product.images.map((img, index) => (
                                    <Image
                                        key={index}
                                        source={{ uri: `${urlImgProduct}/${img}` }}
                                        style={imgStyle}
                                    />
                                ))}
                            </ScrollView>
                            <View style={wrapInfoProduct}>
                                <View style={infoProduct}>
                                    <Text 
                                        style={nameProduct}
                                    >
                                        {global.toTitleCase(`${product.name}`)}
                                    </Text>
                                    <Text style={{ color: '#7E5AC8', fontSize: 18 }}>/</Text>
                                    <Text style={priceProduct}>{product.price}$</Text>
                                </View>
                                <View style={descProduct}>
                                    <Text style={txtDescProduct}>
                                        {product.description}
                                    </Text>
                                </View>
                                <View style={bottomProduct}>
                                    <Text
                                        style={txtName}
                                    >
                                        Material: {global.toTitleCase(`${product.material}`)}
                                    </Text>
                                    <View />
                                    <View style={rightBottomProduct}>
                                        <Text style={txtColor}>Color: {product.color}</Text>
                                        <View style={{ width: 5 }} />
                                        <View
                                            style={{
                                                width: 16,
                                                height: 16,
                                                borderRadius: 8,
                                                backgroundColor: `${product.color.toLowerCase()}`,
                                                borderWidth: 1,
                                                borderColor: '#CE4D8D'
                                            }}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                }
            </View>
        );
    }
}

