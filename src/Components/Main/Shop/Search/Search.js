import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';

//  STYLES
import { sSearch } from '../../../../Styles/rootStyles';

//  GLOBAL
import global from '../../../global';

//  CONFIGURE
import { urlImgProduct } from '../../../../configure';

export default class Search extends Component {
    goBack() {
        const { navigation } = this.props;
        navigation.goBack();
    }
    gotoDetail() {
        const { navigation } = this.props;
        navigation.navigate('MH_ProductDetail');
    }
    render() {
        const {
            container, productContainer,
            productInfo, productImage, lastRowInfo,
            txtName, txtPrice, txtMaterial, txtColor, txtShowDetail,
        } = sSearch;
        const { params } = this.props.navigation.state;
        const listSearch = params || [];
        console.log(listSearch);
        return (
            <ScrollView style={container}>
                {listSearch.map(product => (
                    <View key={product.id} style={productContainer}>
                        <Image
                            source={{ uri: `${urlImgProduct}/${product.images[0]}` }}
                            style={productImage}
                        />
                        <View style={productInfo}>
                            <Text style={txtName}>{global.toTitleCase(product.name)}</Text>
                            <Text style={txtPrice}>{product.price}$</Text>
                            <Text style={txtMaterial}>Material {product.meterial}</Text>
                            <View style={lastRowInfo}>
                                <Text style={txtColor}>Color {product.color}</Text>
                                <View
                                    style={{
                                        backgroundColor: product.color.toLowerCase(),
                                        height: 16,
                                        width: 16,
                                        borderRadius: 8
                                    }}
                                />
                                <TouchableOpacity onPress={() => { this.gotoDetail(); }}>
                                    <Text style={txtShowDetail}>SHOW DETAILS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                ))}
            </ScrollView>
        );
    }
}
