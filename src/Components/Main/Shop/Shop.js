import React, { Component } from 'react';
import {
    View
} from 'react-native';

import { Tabbar } from '../../../Navigation/Navigation';
import Header from './Header';

//API
import initData from '../../../API/initData';
import saveCart from '../../../API/saveCart';
import getCart from '../../../API/getCart';

//  GLOBAL
import global from '../../global';

class Shop extends Component {  
    constructor(props) {
        super(props);
        this.state = {
            types: [],
            products: [],
            cartArray: []
        };
        global.addProductToCart = this.addProductToCart.bind(this);
        global.incrQuantity = this.incrQuantity.bind(this);
        global.decrQuantity = this.decrQuantity.bind(this);     
        global.removeProduct = this.removeProduct.bind(this);  
    } 
    componentDidMount() {
        initData()
        .then(resJSON => {
            const { type, product } = resJSON;
            this.setState({ 
                types: type,
                products: product
            });
        });
        getCart().then(cartArray => { this.setState({ cartArray }); });
    }
    componentWillUpdate(nextProps, nextState) {
        global.cartArray = nextState.cartArray;
    }
    addProductToCart(product) {
        this.setState({
            cartArray: this.state.cartArray.concat({ product, quantity: 1 })
        }, () => {
            saveCart(this.state.cartArray);
        });
    }
    incrQuantity(productID) {
        const newCart = this.state.cartArray.map(e => {
            if (e.product.id !== productID) return e;
            return { product: e.product, quantity: e.quantity + 1 };
        });
        this.setState({ cartArray: newCart }, () => {
            saveCart(this.state.cartArray);
        });
    }
    decrQuantity(productID) {
        const newCart = this.state.cartArray.map(e => {
            if (e.product.id !== productID) return e;
            return { product: e.product, quantity: e.quantity - 1 };
        });
        this.setState({ cartArray: newCart }, () => {
            const checkZeroQuantity = this.state.cartArray.some(e => e.quantity === 0);
            if (checkZeroQuantity === true) {
                this.setState({
                    cartArray: this.state.cartArray.filter(e => e.product.id !== productID)
                }, () => { saveCart(this.state.cartArray); });
            } else {
                this.setState({ cartArray: newCart }, () => {
                    saveCart(this.state.cartArray);
                });                
            }
        });
    }
    removeProduct(productID) {
        const newCart = this.state.cartArray.filter(e => e.product.id !== productID);
        this.setState({ cartArray: newCart }, () => {
            saveCart(this.state.cartArray);
        });
    }
    openMenu() {
        const { navigation } = this.props;
        navigation.navigate('DrawerOpen');
    }
    render() {
        const { types, products, cartArray } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <Header 
                    openMenu={this.openMenu.bind(this)}
                />
                <Tabbar 
                    screenProps={{ 
                        types, 
                        products,
                        cartArray
                    }}
                />
            </View>
        );
    }
}
export default Shop;

