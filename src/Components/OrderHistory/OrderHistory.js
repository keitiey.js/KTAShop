import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';

//  STYLES
import { sOrderHistory } from '../../Styles/rootStyles';
//  IMAGES
import icBacks from '../../Media/appIcon/backs.png';

export default class OrderHistory extends Component {
    render() {
        const {
            container, header, body, iconHeader, txtHeader,
            wrapper, leftContent, rightContent, txtLeftContent,
            txtID, txtTime, txtStatus, txtPrice
        } = sOrderHistory;
        return (
            <View style={container}>
                <View style={header}>
                    <View />
                    <Text style={txtHeader}>Order History</Text>
                    <TouchableOpacity>
                        <Image source={icBacks} style={iconHeader} />
                    </TouchableOpacity>
                </View>
                <View style={body}>
                    <View style={wrapper}>
                        <View style={leftContent}>
                            <Text style={txtLeftContent}>Order id:</Text>
                            <Text style={txtLeftContent}>Order time:</Text>
                            <Text style={txtLeftContent}>Status:</Text>
                            <Text style={txtLeftContent}>Total:</Text>
                        </View>
                        <View />
                        <View style={rightContent}>
                            <Text style={txtID}>ORD15</Text>
                            <Text style={txtTime}>2017-04-19 08:30:13</Text>
                            <Text style={txtStatus}>Pending</Text>
                            <Text style={txtPrice}>392$</Text>
                        </View>
                    </View>
                    <View style={wrapper}>
                        <View style={leftContent}>
                            <Text style={txtLeftContent}>Order id:</Text>
                            <Text style={txtLeftContent}>Order time:</Text>
                            <Text style={txtLeftContent}>Status:</Text>
                            <Text style={txtLeftContent}>Total:</Text>
                        </View>
                        <View />
                        <View style={rightContent}>
                            <Text style={txtID}>ORD16</Text>
                            <Text style={txtTime}>2017-04-19 08:30:39</Text>
                            <Text style={txtStatus}>Pending</Text>
                            <Text style={txtPrice}>107$</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
