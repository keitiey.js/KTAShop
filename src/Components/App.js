import React, { Component } from 'react';

import { ShopStack } from '../Navigation/Navigation';

export default class App extends Component {
    render() {
        return (
            <ShopStack />
        );
    }
}
