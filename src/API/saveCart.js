import { AsyncStorage } from 'react-native';

const saveCart = async (cartArray) => {
    try {
        await AsyncStorage.setItem('@cart', JSON.stringify(cartArray));        
    } catch (e) {
        console.log(e);
    }
};

export default saveCart;
