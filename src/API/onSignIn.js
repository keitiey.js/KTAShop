import { urlSignIn } from '../configure';

const onSignIn = (email, password) => (
    fetch(urlSignIn, {  //eslint-disable-line
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify({ email, password })
    })
    .then(res => res.json())
);

export default onSignIn;
