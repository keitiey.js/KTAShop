import { rootURL } from '../configure';

const initData = () => (
    fetch(`${rootURL}/api/`)    //eslint-disable-line
    .then(res => res.json())    
);

export default initData;

