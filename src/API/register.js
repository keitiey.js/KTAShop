import { urlRegister } from '../configure';

const register = (email, name, password) => (
    fetch(urlRegister, {    //eslint-disable-line
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify({ email, name, password })
    }).then(res => res.text())    
);

export default register;
