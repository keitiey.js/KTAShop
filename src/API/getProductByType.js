import { urlProductByType } from '../configure';

const getProductByType = (idType, page) => (
    fetch(`${urlProductByType}?id_type=${idType}&page=${page}`) //eslint-disable-line
    .then(res => res.json())
);

export default getProductByType;
