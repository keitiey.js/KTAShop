import { urlSearch } from '../configure';

const getSearchList = (key) => (
    fetch(`${urlSearch}?key=${key}`)    // eslint-disable-line
    .then(res => res.json())
);

export default getSearchList;
