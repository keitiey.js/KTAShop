import { urlChangeInfo } from '../configure';

const changeInfo = (token, name, address, phone) => (
    fetch(urlChangeInfo, {  //eslint-disable-line
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify({ token, name, address, phone })
    })
    .then(res => res.json())
);

export default changeInfo;
