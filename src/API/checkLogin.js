import { urlCheckLogin } from '../configure';

const checkLogin = (token) => (
    fetch(urlCheckLogin, { //eslint-disable-line
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify({ token })
    })
    .then(res => res.json())
);

export default checkLogin;
