const rootURL = 'http://192.168.100.124';
module.exports = {
    rootURL,
    urlImgType: `${rootURL}/api/images/type`,
    urlImgProduct: `${rootURL}/api/images/product`,
    urlRegister: `${rootURL}/api/register.php`,
    urlSignIn: `${rootURL}/api/login.php`,
    urlCheckLogin: `${rootURL}/api/check_login.php`,
    urlChangeInfo: `${rootURL}/api/change_info.php`,
    urlProductByType: `${rootURL}/api/product_by_type.php`,
    urlSearch: `${rootURL}/api/search.php`,
};
