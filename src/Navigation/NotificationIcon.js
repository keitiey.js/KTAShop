import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';

class NotificationIcon extends Component {
    render() {
        const { notifications } = this.props;
        return (
            <View
                style={{
                    zIndex: 0,
                    flex: 1,
                    alignSelf: 'stretch',
                    justifyContent: 'space-around',
                    alignItems: 'center'
                }}
            >
                <Image source={this.props.sourceIcon} style={this.props.styleProps} />
                {
                    notifications.length > 0 ?
                        <View
                            style={{
                                position: 'absolute',
                                top: 0,
                                right: 0,
                                width: 16,
                                height: 16,
                                borderRadius: 8,
                                backgroundColor: '#007AFF',
                                borderColor: '#FFFFFF',
                                borderWidth: 1,
                                zIndex: 2,
                                alignItems: 'center'
                            }}
                        >
                            <Text
                                style={{ color: '#FFFFFF', fontSize: 10 }}
                            >
                                {notifications.length}
                            </Text>
                        </View>
                        : undefined
                }
            </View>
        );
    }
}

module.exports = NotificationIcon;
