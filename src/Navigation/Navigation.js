import React from 'react';
import { Image, Dimensions } from 'react-native';
import {
    StackNavigator,
    TabNavigator,
    DrawerNavigator
} from 'react-navigation';

//  STYLES
import { sIcon } from '../Styles/rootStyles';

//  IMAGES
import icHome from '../Media/appIcon/home.png';
import icCart from '../Media/appIcon/cart.png';
import icSearch from '../Media/appIcon/search.png';
import icContact from '../Media/appIcon/contact.png';

//  Screen ShopStack
import Main from '../Components/Main/Main';
import Authentication from '../Components/Authentication/Authentication';
import ChangeInfo from '../Components/ChangeInfo/ChangeInfo';
import OrderHistory from '../Components/OrderHistory/OrderHistory';

//  Screen HomeStack
import Home from '../Components/Main/Shop/Home/Home';
import ListProduct from '../Components/Main/Shop/ListProduct/ListProduct';
import ProductDetail from '../Components/Main/Shop/ProductDetail/ProductDetail';

//  Screens Tabbar
import Cart from '../Components/Main/Shop/Cart/Cart';
import Contact from '../Components/Main/Shop/Contact/Contact';
import Search from '../Components/Main/Shop/Search/Search';

//  Screen DrawerShopToMenu
import Shop from '../Components/Main/Shop/Shop';
import Menu from '../Components/Main/Menu/Menu';

//  BADGE ICON
import NotificationIcon from './NotificationIcon'; 

//  GLOBAL
import global from '../Components/global';

const { width } = Dimensions.get('window');


//  STACK NAVIGATOR
export const ShopStack = StackNavigator({
    MH_Main: {
        screen: Main
    },
    MH_OrderHistory: {
        screen: OrderHistory
    },
    MH_ChangeInfo: {
        screen: ChangeInfo
    },
    MH_Authentication: {
        screen: Authentication
    }
}, {
        headerMode: 'none',
        mode: 'modal'
    });


const HomeStack = StackNavigator({
    MH_Home: {
        screen: Home
    },
    MH_ProductDetail: {
        screen: ProductDetail
    },
    MH_ListProduct: {
        screen: ListProduct
    },
}, {
        headerMode: 'none',
        mode: 'modal'
    });

const CartStack = StackNavigator({
    MH_Cart: {
        screen: Cart
    },
    MH_ProductDetail: {
        screen: ProductDetail
    }
}, {
        headerMode: 'none',
        mode: 'modal'
    });

const SearchStack = StackNavigator({
    MH_Search: {
        screen: Search
    },
    MH_ProductDetail: {
        screen: ProductDetail
    }
}, {
        headerMode: 'none',
        mode: 'modal'
    });

//  TAB NAVIGATOR
export const Tabbar = TabNavigator({
    Home: {
        screen: HomeStack,
        navigationOptions: {
            tabBarLabel: 'Home',
            tabBarIcon: ({ tintColor }) =>
                (<Image
                    source={icHome}
                    style={[sIcon, { tintColor }]}
                />)
        }
    },
    Cart: {
        screen: CartStack,
        navigationOptions: {
            tabBarLabel: 'Cart',
            tabBarIcon: ({ tintColor }) => (
                <NotificationIcon 
                    notifications={global.cartArray} 
                    styleProps={[sIcon, { tintColor }]}
                    sourceIcon={icCart}
                />
            ),
        }
    },
    Search: {
        screen: SearchStack,
        navigationOptions: {
            tabBarLabel: 'Search',
            tabBarIcon: ({ tintColor }) =>
                (<Image
                    source={icSearch}
                    style={[sIcon, { tintColor }]}
                />)
        }
    },
    Contact: {
        screen: Contact,
        navigationOptions: {
            tabBarLabel: 'Contact',
            tabBarIcon: ({ tintColor }) =>
                (<Image
                    source={icContact}
                    style={[sIcon, { tintColor }]}
                />)
        }
    },
}, {
        tabBarOptions: {
            activeTintColor: '#2ABB9C',
            inactiveTintColor: '#B0B0B0',
            upperCaseLabel: false,
            showIcon: true,
            showLabel: false,
            style: {
                backgroundColor: '#F8F8F8',
                height: 45
            },
            indicatorStyle: {
                backgroundColor: '#2ABB9C',
            },
            labelStyle: {
                color: '#B0B0B0'
            },
            iconStyle: {
                width: 27,
                height: 27
            }
        },
        tabBarPosition: 'bottom'
    });

//  DRAWER NAVIGATOR
export const DrawerShopToMenu = DrawerNavigator({
    Shop: { screen: Shop }
}, {
        drawerWidth: width / 1.5,
        drawerPosition: 'left',
        contentComponent: (props) => <Menu {...props} />
    }
);
