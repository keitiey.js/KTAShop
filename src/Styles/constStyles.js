import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
//  TOP PRODUCT
const widthProduct = (width - 60) / 2;
const heightProductImg = (widthProduct / 361) * 452;
//  CATEGORY
const widthImgCategory = width - 40;
const heightImgCategory = widthImgCategory / 2;
//  COLLECTION
const widthImgCollection = width - 40;
const heightImgCollection = (widthImgCollection / 933) * 465;
//  PRODUCT DETAIL
const widthImgProductDetail = (width - 40 - 10) / 2;
const heightImgProductDetail = (widthImgProductDetail / 361) * 452;
//  CART
const widthImgCart = (width - 10 - 10 - 10 - 10) / 4;
const heightImgCart = (widthImgCart / 361) * 452;
//  CONTACT
const widthMap = width - 20;
const heightMap = height / 2;

module.exports = {
    width,
    height,
    colorPrice: '#C22171',
    constMargin: 10,
    constPadding: 10,
    constFontSizeTitle: 20,
    backgroundColor: '#2ABB9C',
    colorTitle: '#AFAEAF',
    fontFamily: 'Avenir',
    product: {
        widthProduct,
        heightProductImg,
    },
    category: {
        widthImg: widthImgCategory,
        heightImg: heightImgCategory
    },
    collection: {
        widthImg: widthImgCollection,
        heightImg: heightImgCollection
    },
    productDetail: {
        widthImg: widthImgProductDetail,
        heightImg: heightImgProductDetail
    },
    cart: {
        widthImg: widthImgCart,
        heightImg: heightImgCart
    },
    contact: {
        widthMap,
        heightMap
    }
};
