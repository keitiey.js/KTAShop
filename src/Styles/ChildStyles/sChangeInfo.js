import { StyleSheet } from 'react-native';
import { 
    backgroundColor,
    height,
    // product,
    // colorTitle,
    constMargin,
    constPadding
    // constFontSizeTitle
} from '../constStyles';

export const sChangeInfo = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F6F6',
    },
    header: {
        backgroundColor,
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: height / 10,
        alignItems: 'center',
        paddingHorizontal: constPadding,
        elevation: 7 
    },
    txtHeader: {
        color: '#FFFFFF',
        fontSize: 18
    },
    iconHeader: {
        width: 30, height: 30,
    },
    body: {
        flex: 1,
        paddingHorizontal: constPadding,
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrapInput: {
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        marginHorizontal: constMargin,
    },
    inputText: {
        borderColor: backgroundColor,
        borderWidth: 1,
        marginVertical: constMargin,
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        paddingLeft: constPadding * 2,
    },
    btnChange: {
        backgroundColor,
        borderRadius: 20,
        paddingVertical: constPadding,
        alignItems: 'center',
        height: 50,
        justifyContent: 'center'
    }
});
