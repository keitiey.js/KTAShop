import { StyleSheet } from 'react-native';
import {
    contact,
    colorPrice
} from '../constStyles.js';

export const sContact = StyleSheet.create({
    container: {
        flex: 1, padding: 10
    },
    wrapMap: {
        flex: 1, marginBottom: 5, elevation: 7, backgroundColor: '#FFFFFF'
    },
    mapStyle: {
        width: contact.widthMap, height: contact.heightMap
    },
    wrapInfo: {
        flex: 1, marginTop: 5, padding: 10, backgroundColor: '#FFFFFF', elevation: 7
    },
    iconStyle: {
        width: 25, height: 25
    },
    txtStyle: {
        color: colorPrice,
        fontSize: 17,
        fontWeight: '300'
    },
    wrapRow: {
        flex: 1, 
        borderBottomColor: '#E0E0E0', 
        borderBottomWidth: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    wrapLastRow: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'        
    }
});
