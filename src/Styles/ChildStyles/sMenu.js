import { StyleSheet } from 'react-native';

import {
    backgroundColor,
    constPadding
} from '../constStyles.js';

export const sMenu = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor,
        borderRightWidth: 1,
        borderColor: '#FFF',
        alignItems: 'center'
    },
    loginContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    profileImg: {
        width: 120,
        height: 120,
        borderRadius: 60,
        marginVertical: 30
    },
    buttonStyle: {
        height: 50,
        paddingHorizontal: 70,
        backgroundColor: '#FFF',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyleSignIn: {
        height: 50,
        width: 200,
        backgroundColor: '#FFF',
        borderRadius: 5,
        marginBottom: 10,
        justifyContent: 'center',
        paddingLeft: constPadding
    },
    buttonText: {
        color: backgroundColor,
        fontSize: 20
    },
    buttonTextSignIn: {
        color: backgroundColor,
        fontSize: 15
    },
    usernameStyle: {
        color: '#FFF',
        fontSize: 20
    }
});
