import { StyleSheet } from 'react-native';
import {
    height,
    constFontSize,
    constPadding,
    constMargin,
    backgroundColor
} from '../constStyles.js';

export const sHeader = StyleSheet.create({
    wrapper: { 
        height: height / 8, 
        backgroundColor, 
        padding: constPadding, 
        justifyContent: 'space-around' 
    },
    row1: { 
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center' 
    },
    textInput: { 
        height: height / 17, 
        backgroundColor: '#FFF',
        paddingLeft: constPadding,
        marginTop: constMargin,
        paddingVertical: 0
    },
    iconStyle: { 
        width: 25, 
        height: 25 
    },
    titleStyle: {
        color: '#FFFFFF',
        fontSize: constFontSize
    }    
});

