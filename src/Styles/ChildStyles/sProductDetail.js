import { StyleSheet } from 'react-native';
import { 
    // colorTitle,
    height,
    constMargin,
    constPadding,
    productDetail
    // constFontSizeTitle
} from '../constStyles';

export const sProductDetail = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#D6D6D6'
    },
    wrapper: {
        backgroundColor: '#FFFFFF',
        margin: constMargin,
        marginVertical: constMargin / 2,
        borderRadius: 5,
        elevation: 7
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: constPadding,
        height: height / 12
    }, 
    btnBack: {
        width: 25, height: 25
    },
    btnCartFull: {
        width: 25, height: 25
    },
    content: {
        padding: constPadding
    },
    imageProduct: {
        flexDirection: 'row'
    },
    wrapInfoProduct: {
        paddingHorizontal: constPadding
    },
    infoProduct: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: height / 12,
        borderBottomColor: '#F7F7F7',
        borderBottomWidth: 1
    },
    nameProduct: {
        color: '#3F3F46',
        fontWeight: '500',
        fontSize: 20,
        marginRight: constMargin
    },
    priceProduct: {
        color: '#A3A3A3',
        fontSize: 20,
        marginLeft: constMargin
    },
    imgStyle: {
        width: productDetail.widthImg,
        height: productDetail.heightImg,
        marginHorizontal: constMargin / 2
    },
    descProduct: {
        paddingTop: constPadding
    },
    txtDescProduct: {
        color: '#B1B1B1',
        fontSize: 15
    },
    bottomProduct: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: constPadding
    },
    rightBottomProduct: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    txtName: {
        color: '#CE4D8D',
        fontSize: 15
    },
    txtColor: {
        color: '#CE4D8D',
        fontSize: 15
    }
});
