import { StyleSheet } from 'react-native';
import { 
    product,
    colorTitle,
    constMargin,
    constPadding,
    constFontSizeTitle
} from '../constStyles';

export const sTopProduct = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        margin: constMargin,
        elevation: 7
    },
    titleContainer: {
        height: 50,
        justifyContent: 'center'
    },
    title: {
        marginLeft: constMargin,
        color: colorTitle,
        fontSize: constFontSizeTitle
    },
    bodyContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        paddingBottom: constPadding
    },
    productContainer: {
        width: product.widthProduct,
        elevation: 3,
        backgroundColor: '#fcfcfc'
    },
    productImg: {
        width: product.widthProduct,
        height: product.heightProductImg
    },
    productName: {
        paddingLeft: constPadding,
        color: '#DFDEDC',
        fontWeight: '500',
        marginVertical: 5
    },
    productPrice: {
        paddingLeft: 10,
        color: '#705697',
        fontWeight: '500',
        marginBottom: constMargin / 2
    }
});
