import { StyleSheet } from 'react-native';
import {
    // backgroundColor,
    constPadding,
    colorPrice,
    constMargin
} from '../constStyles.js';

export const sSearch = StyleSheet.create({
    // container: {
    //     flex: 1, 
    //     backgroundColor: '#DFDEDC',        
    //     padding: constPadding
    // },
    container: {
        backgroundColor: '#DFDEDC',
    },
    backStyle: {
        width: 30, height: 30
    },
    titleStyle: {
        color: colorPrice,
        fontSize: 20,
    },
    productContainer: {
        backgroundColor: '#FFFFFF',
        padding: constPadding,
        flexDirection: 'row',
        borderTopColor: '#F2F2F2',
        borderTopWidth: 1, 
        margin: constMargin,
        elevation: 2
    },
    productInfo: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'space-between'
    },
    productImage: {
        width: 90,
        height: (90 * 452) / 361,
    },
    lastRowInfo: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtName: {
        color: '#AEAEAE',
        fontSize: 20,
        fontWeight: '400'
    },
    txtPrice: {
        color: colorPrice,    
    },
    txtMaterial: {

    },
    txtColor: {
        
    },
    txtShowDetail: {
        fontSize: 11,
        color: colorPrice
    }
});
