import { StyleSheet } from 'react-native';
import {
    backgroundColor,
    constPadding,
    constMargin
} from '../constStyles.js';

export const sAuthentication = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor,
        padding: constPadding * 2,
        justifyContent: 'space-between'
    },
    row1: { 
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    iconStyle: { 
        width: 30, 
        height: 30 
    },
    titleStyle: {
        color: '#FFFFFF',
        fontSize: 25
    },
    wrapControl: {
        flexDirection: 'row',
        // width: 340
        alignSelf: 'stretch'
    },
    signInStyle: {
        backgroundColor: '#FFF',
        flex: 1,
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20,
        marginRight: 1,
    },
    signUpStyle: {
        backgroundColor: '#FFF',
        flex: 1,
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomRightRadius: 20,
        borderTopRightRadius: 20,
        marginLeft: 1,    
    },
    activeStyle: {
        color: backgroundColor
    },
    inactiveStyle: {
        color: '#DADADA'
    },
    inputStyle: {
        height: 50,
        backgroundColor: '#FFF',
        marginBottom: constMargin,
        borderRadius: 20,
        paddingLeft: 30
    },
    bigButton: {
        height: 50,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: '#FFF',
        fontWeight: '400'
    }    
});
