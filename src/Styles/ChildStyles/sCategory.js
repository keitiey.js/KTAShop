import { StyleSheet } from 'react-native';
import { 
    category,
    height,
    colorTitle,
    constMargin,
    constPadding
} from '../constStyles';

export const sCategory = StyleSheet.create({
    wrapper: {
        height: height * 0.36,
        backgroundColor: '#FFF',
        margin: constMargin,

        // IOS
        // shadowColor: '#2E272B',
        // shadowOffset: { width: 0, height: 3 },
        // shadowOpacity: 0.2,

        //  Android
        elevation: 7,

        padding: constPadding,
        paddingTop: 0
    },
    textStyle: {
        fontSize: 17,
        color: colorTitle
    },
    imageStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        width: category.widthImg,
        height: category.heightImg
    }
});
