import { StyleSheet } from 'react-native';
import {
    height,
    backgroundColor,
    constMargin,
    constPadding,
    cart,
    colorPrice
} from '../constStyles.js';

export const sCart = StyleSheet.create({
    container: {
        flex: 1, 
        margin: constMargin,
    },
    listProduct: {
        flex: 16
    },
    checkOutNow: {
        flex: 2,
        backgroundColor,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtCheckOutNow: {
        color: '#FFFFFF',
        fontWeight: '500'
    },
    wrapProduct: {
        backgroundColor: '#FFFFFF',
        height: height / 4.7,
        marginBottom: 20,
        elevation: 4,
        borderRadius: 3,
        padding: constPadding,
        flexDirection: 'row'
    },
    imgStyle: {
        width: cart.widthImg,
        height: cart.heightImg
    },
    detailProduct: {
        justifyContent: 'space-between',
        paddingLeft: constPadding,
        flex: 1
    },
    header: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    txtName: {
        color: '#ACACAC',
        fontSize: 18
    },
    txtPrice: {
        color: colorPrice,
        fontSize: 20
    },
    middle: {
        flex: 1, 
        justifyContent: 'center'
    },
    bottom: {
        flex: 1, 
        flexDirection: 'row'
    },
    txtControlNumber: {
        fontSize: 19, fontWeight: '400'
    },
    txtShowDetails: {
        fontWeight: '100',
        color: colorPrice,
        fontSize: 13
    },
    wrapControlNumber: {
        flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end'
    },
    wrapBtnShowDetails: {
        flex: 2, 
        alignItems: 'flex-end', 
        justifyContent: 'flex-end'
    }
});
