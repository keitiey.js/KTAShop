import { StyleSheet } from 'react-native';
import { 
    backgroundColor,
    height,
    // product,
    // colorTitle,
    constMargin,
    constPadding
    // constFontSizeTitle
} from '../constStyles';

export const sOrderHistory = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F6F6',
    },
    header: {
        backgroundColor,
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: height / 10,
        alignItems: 'center',
        paddingHorizontal: constPadding,
        elevation: 7 
    },
    txtHeader: {
        color: '#FFFFFF',
        fontSize: 18
    },
    iconHeader: {
        width: 30, height: 30,
    },
    body: {
        padding: constPadding
    },
    wrapper: {
        backgroundColor: '#FFFFFF',
        height: height / 5,
        marginBottom: constMargin,
        padding: constPadding,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    leftContent: {
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    rightContent: {
        justifyContent: 'space-between',
        alignItems: 'flex-end'
    },
    txtLeftContent: {
        fontWeight: '500',
        color: '#9B9B9B'
    },
    txtID: {
        color: '#2FBC9E'
    },
    txtTime: {
        color: '#C22171',
        fontWeight: '100'
    },
    txtStatus: {
        color: '#2FBC9E'
    }, 
    txtPrice: {
        color: '#C22171',
        fontWeight: '500',
        fontSize: 15
    }
});
