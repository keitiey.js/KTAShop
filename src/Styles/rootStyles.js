import { StyleSheet } from 'react-native';

import { sHeader } from './ChildStyles/sHeader';
import { sTopProduct } from './ChildStyles/sTopProduct';
import { sCategory } from './ChildStyles/sCategory';
import { sCollection } from './ChildStyles/sCollection';
import { sMenu } from './ChildStyles/sMenu';
import { sListProduct } from './ChildStyles/sListProduct';
import { sOrderHistory } from './ChildStyles/sOrderHistory';
import { sChangeInfo } from './ChildStyles/sChangeInfo';
import { sProductDetail } from './ChildStyles/sProductDetail';
import { sCart } from './ChildStyles/sCart';
import { sSearch } from './ChildStyles/sSearch';
import { sContact } from './ChildStyles/sContact';

const sIcon = StyleSheet.create({
    style: {
        width: 25,
        height: 25
    }
});

module.exports = {
    sIcon: sIcon.style,
    sHeader,
    sTopProduct,
    sCategory,
    sCollection,
    sMenu,
    sListProduct,
    sOrderHistory,
    sChangeInfo,
    sProductDetail,
    sCart,
    sSearch,
    sContact
};
